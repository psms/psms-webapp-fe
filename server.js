// server.js
const express = require('express');
const path = require('path');
const server = express();

server.use(express.static(path.join(__dirname, 'dist')));

server.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname, 'dist', 'index.html'));
});

server.listen(8080)
