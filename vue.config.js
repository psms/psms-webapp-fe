module.exports = {

    chainWebpack: config => {
    // Markdown loader - uses marked and outputs the file in html format, so it needs the html-loader as well
    config.module
        .rule('markdown')
        .test(/\.md$/)
        .use('html-loader')
            .loader('html-loader')
            .end()
        .use('markdown-loader')
            .loader('markdown-loader')
            .end(),
    // HTML loader
    config.module
        .rule('html')
        .test(/\.html$/)
        .use('html-loader')
        .loader('html-loader')
        .end()
    }
  }