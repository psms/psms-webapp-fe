import { createWebHistory, createRouter } from "vue-router";

//Importing components
import MyOrders from "@/components/MyOrders.vue";
import Order from "@/components/OrderPage.vue";
import OrderDetails from "@/components/OrderDetails.vue";
import UserPage from '@/components/UserPage'
import UserSectionSummaryPage from '@/components/UserSectionSummaryPage'
import ViewOrder from '@/components/ViewOrder'
import UserAccount from '@/components/UserAccount'
// import testSSO from "@/components/SSOtest.vue"


const routes = [
  {
    path: "/distribution/myorders",
    name: "MyOrders",
    component: MyOrders,
  },
  {
    path: "/account",
    name: "User",
    component: UserAccount,
  },
  {
    path: "/distribution",
    name: "Distribution",
    component: UserSectionSummaryPage,
    props:{url:"/distribution",file_path:""},
  },
  {
    path: "/distribution/order",
    name: "Order",
    component: Order,
  },
  {
    path: "/distribution/orderdetails",
    name: "OrderDetails",
    component: OrderDetails,
  },
  {
    path: "/distribution/myorders/:id",
    name: "ViewOrder",
    component: ViewOrder,
    // meta:{navbar_visibility : false}
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
