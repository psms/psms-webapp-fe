import { createStore } from "vuex" 

// const process.env.VUE_APP_URL = "http://127.0.0.1:5000/";
const headers = { Accept: "application/json" };

var _ = require('lodash');

// var partInBasket = {
//   part,
//   quantity,
//   userInputs
// };

const store = createStore({
  state: {
    currentBasket: [],
    allParts: {},
    allPackages: {},
    documentation: "",
    orderConfig: {},
    partCategories: {},
    userDetails: {}
  },
  mutations: {
    // syncronous
    addPartToBasket (state, payload) {
      // Check if the part is already in the basket with the selected options. 
      // If it is, just increase the quantity of the already existing part.
      var partAlreadyInBasket = false;
      for(var i = 0; i < state.currentBasket.length; i++) 
      {
        var part = state.currentBasket[i];
        if ( payload.part_id == part.part_id )
        {
          var omitQuantity = o => _.omit(o, 'quantity');
          var intersection = _.intersectionWith( _.map(payload, omitQuantity), _.map(part, omitQuantity), _.isEqual);
          console.log(omitQuantity)
          console.log("intersection", intersection)
          if (intersection) {
            state.currentBasket[i].quantity += payload.quantity; 
            state.currentBasket[i].price += payload.price;
            partAlreadyInBasket = true;
            break;
          }
        }
      }
      if ( !partAlreadyInBasket ) { state.currentBasket.push(payload); }
      localStorage.setItem("basketJSON", JSON.stringify(state.currentBasket));
      console.log("basket", state.currentBasket);
    },
    removePartFromBasket (state, payload) {
      var index = state.currentBasket.indexOf(payload);
      state.currentBasket.splice(index, 1);
      localStorage.setItem("basketJSON", JSON.stringify(state.currentBasket));
      console.log("basket ", state.currentBasket);
      console.log("removed part ", payload);
    },
    emptyBasket (state) {
      state.currentBasket = []
      localStorage.setItem("basketJSON", JSON.stringify(state.currentBasket));
    },
    setParts (state, payload) {
      state.allParts = payload;
      console.log('state.allParts', state.allParts)
    },
    setPackages (state, payload) {
      state.allPackages = payload;
    },
    setDocumentation (state, payload) {
      state.documentation = payload;
    },
    setOrderConfig (state, payload) {
      state.orderConfig = payload;
    },
    setCategories (state, payload) {
      state.partCategories = payload;
      console.log('state.partCategories', state.partCategories)
      console.log('state.partCategories', typeof(state.partCategories))

    },
    setUserDetails (state, payload) {
      state.userDetails = payload;
    },
    initializeBasket(state) {
      if ( localStorage.getItem("basketJSON") == null ) {
        console.log("Null localStorage");
        state.currentBasket = []
      }
      else {
        state.currentBasket = JSON.parse(localStorage.getItem("basketJSON"));
      }
    }
  },
  actions: {
    //asyncronous
    async addPartToBasket(state, part) {
      state.commit("addPartToBasket", part);
    },
    async removePartFromBasket(state, part) {
      state.commit("removePartFromBasket", part);
    },
    async emptyBasket(state) {
      state.commit("emptyBasket");
    },
    async setParts(state) {
      console.log('setParts')      
      return new Promise(
        (resolve, reject) => {
          fetch(process.env.VUE_APP_URL+'/restapi/get_parts', {headers}).then(
            (partsRequest) => 
            {
              partsRequest.json().then(
                (parts) =>
                {
                  state.commit("setParts", parts.parts);
                  // return parts;
                  resolve(parts.parts);
                }
              );
            }
          ).catch(error => { reject(error) });
        }
      );
    },
    async setPackages(state) {
      const packagesRequest = await fetch(process.env.VUE_APP_URL+'/restapi/get_packages', {headers});
      var packages = await packagesRequest.json();
      console.log(packages)
      state.commit("setPackages", packages);
    },
    async setDocumentation(state) {
      const documentationRequest = await fetch(process.env.VUE_APP_URL+'/restapi/get_documentation', {headers});
      var documentation = await documentationRequest.text();
      state.commit("setDocumentation", documentation);
    },
    async setOrderConfig(state) {
      return new Promise(
        (resolve, reject) => {
          fetch(process.env.VUE_APP_URL+'/restapi/order/config', {headers}).then(
              (orderDetailsRequest) => 
              {
                  orderDetailsRequest.json().then(
                      (orderDetails) =>
                      {
                          console.log(orderDetails);
                          state.commit("setOrderConfig", orderDetails.config);
                      }
                  );
              }
          ).catch(error => { reject(error) });
        }
      );
    },
    async setCategories(state) {
      return new Promise(
        (resolve, reject) => {
          fetch(process.env.VUE_APP_URL+'/restapi/get_categories', {headers}).then(
            (categoriesRequest) => 
            {
              categoriesRequest.json().then(
                (categories) =>
                {
                  state.commit("setCategories", categories.categories);
                  resolve(categories.categories);
                }
              );
            }
          ).catch(error => { reject(error) });
        }
      );
    },
    async setUserDetails(state, userDetails) {
      state.commit("setUserDetails", userDetails);
    },
    async initializeBasket(state) {
      state.commit("initializeBasket");
    },
    async initializeStore(state) {
      return new Promise(
        (resolve, reject) => {
          this.dispatch("setParts");
          // this.dispatch("setPackages");
          this.dispatch("setCategories");
          this.dispatch("setOrderConfig");
          this.dispatch("initializeBasket");
          resolve('ok') ;
        }
      );
    }
  },
  modules: {},
  getters: {
    getBasket: (state) => state.currentBasket,
    getParts: (state) => state.allParts,
    getPackages: (state) => state.allPackages,
    getDocumentation: (state) => state.documentation,
    getOrderConfig: (state) => state.orderConfig,
    getUserDetails: (state) => state.userDetails,
    getCategories: (state) => state.partCategories
  }
})

export default store